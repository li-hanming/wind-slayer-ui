const path = require('path')
const { defineConfig, build } = require('vite')
const vue = require('@vitejs/plugin-vue')
const libCss = require('vite-plugin-libcss')
const dts = require('vite-plugin-dts')
const fs = require('fs')
const vueJsx = require('@vitejs/plugin-vue-jsx')

const util = require('util')
const child_process = require('child_process')

const exec = util.promisify(child_process.exec)

const spawn = async (...args) => {
  const { spawn } = child_process
  return new Promise((resolve) => {
    const proc = spawn(...args) // 在node.js中执行shell一般用spawn，实现从主进程的输出流连通到子进程的输出流
    proc.stdout.pipe(process.stdout) // 子进程正常流搭到主进程的正常流
    proc.stderr.pipe(process.stderr) // 子进程错误流插到主进程的错误流
    proc.on('close', () => {
      resolve()
    })
  })
}

const entryDir = path.resolve(__dirname, '../src/components')
const outDir = path.resolve(__dirname, '../lib')

const baseConfig = defineConfig({
  configFile: false,
  publicDir: false,
  plugins: [vue(), libCss(), dts(), vueJsx()],
  esbuild: {
    pure: ['console.log'],
  },
})

const rollupOptions = {
  external: ['vue', 'vue-router'],
  output: {
    globals: {
      vue: 'Vue',
    },
  },
}

const buildAll = async () => {
  await build(
    defineConfig({
      ...baseConfig,
      build: {
        rollupOptions,
        lib: {
          entry: path.resolve(entryDir, 'index.ts'),
          name: 'windslayer-ui',
          fileName: 'wind-ui',
          formats: ['es', 'umd'],
        },
        outDir,
        cssCodeSplit: false,
      },
    }),
  )
}

async function delLib() {
  if (fs.existsSync('./lib')) {
    await exec(`del /F /S /Q lib`)
    await exec(`rd /S /Q lib`)
  }
}

const buildLib = async () => {
  await buildAll()
}

async function publish() {
  const npm = process.platform == 'win32' ? 'npm.cmd' : 'npm'

  if (!process.argv[2] || process.argv[2] == '0') {
    await spawn(npm, ['version', 'patch'], { cwd: `./` })
  } else if (process.argv[2] == '1') {
    await spawn(npm, ['version', 'minor'], { cwd: `./` })
  } else if (process.argv[2] == '2') {
    await spawn(npm, ['version', 'major'], { cwd: `./` })
  }

  await spawn(npm, ['publish'], { cwd: `./` })
}

delLib().then(() => {
  buildLib().then(() => {
    publish()
  })
})
