import { App } from 'vue'
import WindCut from './WindCut.vue'

WindCut.install = (app: App) => {
  app.component(WindCut.__name, WindCut)
}

export default WindCut