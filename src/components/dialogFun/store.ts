import { useDialog } from './type'
import { reactive } from 'vue'

export const store: {
  component: Map<string, any>
  props: Map<string, useDialog.propsType>
} = {
  component: new Map(),
  props: new Map(),
}

export const state = reactive<{
  dialogList: Record<any, any>[]
}>({
  dialogList: [],
})
