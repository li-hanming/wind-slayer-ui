import { Dialog } from './dialogClass'
import DialogApp from './dialogApp.vue'
import { getCurrentInstance, h, render } from 'vue'

// const dialogDiv = document.createElement('div')
// dialogDiv.id = 'dialog-app'
// document.body.appendChild(dialogDiv)
// render(h(DialogApp), dialogDiv)

let rootDom: HTMLElement

export function slayerDialog(component: any, rootID: string = "app") {
  if (!rootDom) {//完成初始化挂载
    rootDom = document.getElementById(rootID)
    if (!rootDom) {//获取不到则抛出错误
      throw new Error('获取不到根节点的dom')
    }
    else{//挂载
      render(h(DialogApp), rootDom)
    }
  }
  let instance: any = getCurrentInstance()
  console.log(instance)
  return new Dialog(component)
}
