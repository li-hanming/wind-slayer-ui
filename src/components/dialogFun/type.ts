export declare namespace useDialog {
  export type propsType = {
    animation?: 'fade' | 'bounce' | 'fade-down'
    alignX?: 'left' | 'center' | 'right'
    alignY?: 'top' | 'center' | 'bottom'
    maskClose?: boolean
    maskColor?: string
    opacity?: number
    isShow?: boolean
  }
}
