import { reactive, ref } from 'vue'
import { useDialog } from './type'
import { state } from './store'

export class Dialog {
  public component: any
  public name: string
  public props: useDialog.propsType = reactive({
    animation: 'fade',
    alignX: 'center',
    alignY: 'center',
    maskClose: false,
    maskColor: '',
    opacity: -1,
    isShow: false,
  })

  constructor(component: any) {
    this.component = component
    this.name = component.__name
  }

  public show(props?: useDialog.propsType) {
    if (props) Object.assign(this.props, props)
    this.props.isShow = true
    state.dialogList.push(this)
  }

  public close() {
    setTimeout(() => {
      state.dialogList = state.dialogList.filter((e) => e.name != this.name)
    }, 500)
  }
}
