import { App } from 'vue'
import WindEnlarge from './WindEnlarge.vue'

WindEnlarge.install = (app: App) => {
  app.component(WindEnlarge.__name, WindEnlarge)
}

export default WindEnlarge