import { App } from 'vue'
import windPagination from './pagination/index'
import windDialog from './dialog/index'
import windCut from './windCut/index'
import WindEnlarge from './windEnlarge/index'
import WindLoading from './windLoading/index'
import VirtualList from './virtualTable/virtualList.vue'
import { slayerDialog } from './dialogFun/index'

const components = [windPagination, windDialog, windCut, WindEnlarge, WindLoading, VirtualList]

const lib = {
  install: (Vue: App) => {
    components.forEach((e: any) => {
      Vue.component('wind-' + e.__name, e)
    })
  },
}

export { windPagination, windDialog, slayerDialog, windCut, WindEnlarge, WindLoading, VirtualList }

export default {
  install(app: App) {
    app.use(lib)
  },
}
