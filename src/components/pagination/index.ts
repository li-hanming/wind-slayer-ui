import { App } from 'vue'
import windPagination from './pagination.vue'

windPagination.install = (app: App) => {
  app.component(windPagination.__name, windPagination)
}

export default windPagination
