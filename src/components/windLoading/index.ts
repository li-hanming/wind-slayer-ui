import { App } from 'vue'
import WindLoading from './WindLoading.vue'

WindLoading.install = (app: App) => {
  app.component(WindLoading.__name, WindLoading)
}

export default WindLoading