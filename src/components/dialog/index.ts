import { App } from 'vue'
import windDialog from './dialog.vue'

windDialog.install = (app: App) => {
  app.component(windDialog.__name, windDialog)
}

export default windDialog