export function throttle(fn: Function, wait: number = 60) {
  let last = 0
  return function (...args: any[]) {
    let now = +new Date()
    if (now - last > wait) {
      fn(...args)
      last = now
    }
  }
}

export function debounce(fn: Function, delay = 500) {
  let timer: any = null

  return function (...args) {
    if (timer) {
      clearTimeout(timer)
      timer = null
    }
    timer = setTimeout(() => {
      fn.apply(this, args)
      clearTimeout(timer)
      timer = null
    }, delay)
  }
}

export function download(url: any, name: string) {
  let a = document.createElement('a')
  a.href = url
  a.target = '_blank'
  a.download = name
  a.click()
}
