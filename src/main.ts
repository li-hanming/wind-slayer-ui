import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// import WindUI from './components/index'

// import WindUI from '../lib/wind-ui.es.js'

// import WindUI from 'windslayer-ui'

// createApp(App).use(WindUI).mount('#app')

createApp(App).use(ElementPlus).mount('#app')
