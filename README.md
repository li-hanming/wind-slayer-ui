# windSlayer-UI

#### 介绍
wind-ui,个人以自己工作项目经验需求而实现的组件库

#### 软件架构
vite+vue3+typescript

#### 安装教程
已上线npm：
<br>
```
npm i windslayer-ui
```

#### 使用说明
 **全局引入：** 
<br>
```
import WindUI from 'windslayer-ui'

createApp(App).use(WindUI).mount('#app')
```


 **修改样式** ：使用的都是‘弱样式’，即只需要增加组件内对应的class的优先级即可修改，嵌套父级选择器即可

后续补充，在个人博客上有写：https://blog.csdn.net/exxes?type=blog
<br>
开源仓库地址：https://gitee.com/li-hanming/wind-slayer-ui/ 别问为什么不用github，问就是上不去

#### 分页器组件
引入：
```
import { windPagination } from 'windslayer-ui'
```
demo示例：
<br>
```
<wind-pagination  
      class="pagination"  
      :total-num="testList.length"  
      :total-list="testList"  
      :show-num="showNum"  
      :one-page-num="onePageNum"  
      @now-list="getNowList"  
      @now-page="getNowPage"  
    ></wind-pagination>
```

| props                        |                                                                             |
|------------------------------|-----------------------------------------------------------------------------|
| total-list : Array<Object>   |  **可选** ，将整个数据列表传递过去，然后可通过监听now-list去获取当前页的数据      |
| show-num : Number            |  **可选** ，可以将每一个页看成一个格子，有多少个格子的意思，默认9个               |
| total-num : Number           |  **必选** ，数据的总条数，需要根据这个总条数来计算多少页和页面的布局(是否需要省略) |
| one-page-num : Number        |  **可选** ，一页有多少条数据，默认50                                           |

| emits                              |                                             |
|------------------------------------|---------------------------------------------|
| now-list( list : Array<Object> )   |  需要配合total-list使用，获取当前页的数据      |
| now-page( page : Number )          |  获取当前的页码                               |

| 样式修改的class  |               |
|-----------------|---------------|
| page            |  最外层div     |
| pre             |  上一页按钮    |
| next            |  下一页按钮    |
| pageItem        |  页码按钮      |

![输入图片说明](src/assets/1656490339745.png)


#### 弹窗组件
 **标签式使用：** 
```
import { windDialog } from 'windslayer-ui'
```
demo示例：
<br>
```
<wind-dialog
      :show="show"
      :mask-close="maskClose"
      :mask-color="maskColor"
      :align-x="alignX"
      :align-y="alignY"
      :opacity="opacity"
      :animation="animation"
    >
      <div class="text">111</div>
</wind-dialog>
```
标签中间是加上自定义的内容的使用了slot

| props                                          |                                                                    |
|------------------------------------------------|--------------------------------------------------------------------|
| show : Boolean                                 |  **必选** ，使用这个来控制弹窗的展示和隐藏，使用函数式调用时不需要传递这个|
| animation : ['fade' , 'bounce' , 'fade-down']  |  **可选** ，弹窗显示的动画，默认fade                                 |
| alignX : ['left' , 'center' , 'right']         |  **可选** ，弹窗在水平方向的位置，默认center                         |
| alignY : ['top' , 'center' , 'bottom']         |  **可选** ，弹窗在垂直方向的位置，默认center                         |
| maskClose : Boolean                            |  **可选** ，点击蒙层能否关闭，默认false                              |
| maskColor : String                             |  **可选** ，蒙层颜色值，默认灰色                                     |
| opacity : Number [0,1]                         |  **可选** ，蒙层透明度，需要maskColor的格式为rgb(xx, xx, xx)才能设置  |


 **函数式使用：** 
```
import { slayerDialog } from 'windslayer-ui'
```
demo示例：
<br>
```
import Test from './test.vue'
slayerDialog(Test).show({maskClose:true...})
slayerDialog(Test).close()
```
| slayerDialog : Function        |  将需要变成弹窗的组件通过这个函数传递                                                      |
|------------------------------------------------|--------------------------------------------------------------------|
| slayerDialog().show(props?)    |  调用这个函数才会展示弹窗，可传递一个对象类型为上面定义的props                               |
| slayerDialog().close()         |  调用这个函数能关闭弹窗，但这个函数用处应该不大，一般都是使用变成弹窗的那个组件去控制关闭       |
| emit('close')                  |  在变成弹窗的那个组件中emit一个close事件即可关闭                                           |


#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 更新日志
v1.3.17 虚拟列表、放大镜和裁剪组件发布

v1.1.15 完成按需引入

v1.1.1  修复了弹窗蒙层透明度的问题

v1.0.37 新增了dialog弹窗组件

v1.0.34 第二个正式版，修复了若干问题

v1.0.10 第一个正式版，完成好打包部署
